import React from 'react';
import './App.module.scss';
import { Switch, Route } from 'react-router-dom';
import Pokedex from '../pokedex/Pokedex';
import Pokemon from '../pokemon/Pokemon';

function App() {
  return (
    <Switch>
      <Route exact path="/" component={Pokedex} /> 
      <Route exact path="/:pokemonId" component={Pokemon} /> 
    </Switch>
  );
}

export default App;
